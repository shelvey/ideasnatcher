//
//  ViewController.h
//  IdeaSnatcher
//
//  Created by Sean Helvey on 3/10/13.
//  Copyright (c) 2013 Sean Helvey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *lblTitle;
@property (nonatomic, retain) IBOutlet UITextField *txtInput;
@property (nonatomic, retain) IBOutlet UIButton *btnSnatch;
@property (nonatomic, retain) IBOutlet UILabel *lblIdea;

- (IBAction) snatchIdea:(id) sender;

@end

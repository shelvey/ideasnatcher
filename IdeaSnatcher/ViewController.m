//
//  ViewController.m
//  IdeaSnatcher
//
//  Created by Sean Helvey on 3/10/13.
//  Copyright (c) 2013 Sean Helvey. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize lblTitle, txtInput, btnSnatch, lblIdea;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) snatchIdea:(id) sender
{
    //GET
    /*
    //NSString* content = [@"item=" stringByAppendingString:txtInput.text];
    NSURL* url = [NSURL URLWithString:@"http://safe-refuge-1958.herokuapp.com/"];
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc]initWithURL:url];  
    [urlRequest setHTTPMethod:@"GET"];
    //[urlRequest setHTTPBody:[content dataUsingEncoding:NSASCIIStringEncoding]];
    
    //get response
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = [[NSError alloc] init];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&urlResponse error:&error];
    
    NSString* newStr = [[NSString alloc] initWithData:responseData
                                             encoding:NSUTF8StringEncoding];
    lblIdea.text = newStr;
    */
    
    //POST -- notice text in line below
    NSString* content = [@"text=" stringByAppendingString:txtInput.text];
    NSURL* url = [NSURL URLWithString:@"http://safe-refuge-1958.herokuapp.com/snatch"];
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc]initWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[content dataUsingEncoding:NSASCIIStringEncoding]];
    
    //get response
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = [[NSError alloc] init];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&urlResponse error:&error];
    
    NSString* newStr = [[NSString alloc] initWithData:responseData
                                             encoding:NSUTF8StringEncoding];
    lblIdea.text = newStr;
}

@end
